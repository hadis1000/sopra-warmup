package model;

import io.Serialization;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import java.io.IOException;
import java.io.Serializable;
import java.util.List;

/**
 * RezeptBuch modelliert ein Rezeptbuch mit beliebig vielen Rezepten.
 */
public class RezeptBuch implements Serializable {

    ObservableList<Rezept> rezepte;

    /**
     * Initialisiert das Rezeptbuch mit den von der Festplatte gelesenen Rezepten.
     * @throws IOException wenn die Rezepte nicht gelesen werden konnten.
     * @throws ClassNotFoundException wenn sich zum Beispiel die serialisierte Klasse geaendert hat.
     */
    public RezeptBuch() throws IOException, ClassNotFoundException {
        Serialization serialization = new Serialization();
        List<Rezept> rezepte = serialization.getRezepte();
        this.rezepte = FXCollections.observableArrayList(rezepte);
    }

    /**
     * Gibt den Inhalt des Rezeptbuches wieder.
     * @return Alle Rezepte
     */
    public ObservableList<Rezept> getRezepte() {
        return this.rezepte;
    }

    /**
     * Erstellt ein neues Standardrezept, fuegt dieses dem Rezeptbuch hinzu und gibt es dann wieder.
     * @return Neues Standardrezept
     */
    public Rezept newRezept() {
        Rezept neuesRezept = new Rezept(
                "Neues Rezept",
                "",
                ""
        );
        add(neuesRezept);
        return neuesRezept;
    }

    /**
     * Fuegt ein Rezept zum Rezeptbuch hinzu.
     * @param rezept Ein Rezept
     */
    public void add(Rezept rezept) {
        this.rezepte.add(rezept);
    }

    /**
     * Entfernt ein Rezept zum Rezeptbuch hinzu
     * und Loescht dieses Rezept von der Festplatte.
     * @param rezept Ein Rezept
     */
    public void remove(Rezept rezept) {
        getRezepte().remove(rezept);
        Serialization serialization = new Serialization();
        serialization.delete(rezept);
    }

    /**
     * Speichert den aktuellen Rezeptbuchinhalt auf der Festplatte.
     * @throws IOException wenn die Rezeptdatei nicht geschriebern werden konnte.
     */
    public void save() throws IOException {
        Serialization serialization = new Serialization();
        serialization.write(getRezepte());

    }

}
