package model;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;

/**
 * Rezept modelliert ein Rezept
 */
public class Rezept implements Serializable {

    private static final long serialVersionUID = 1337L;

    // id ausschliesslich fuer die Serialisierung
    private transient int id;

    // Name, Zutatenliste und Schritte
    private transient StringProperty nameProperty;
    private transient StringProperty zutatenProperty;
    private transient StringProperty schritteProperty;

    /**
     * Konstruktor fuer Rezepte
     * @param name Name
     * @param zutaten Zutaten
     * @param schritte Schritte
     */
    public Rezept(String name, String zutaten, String schritte) {
        this.nameProperty = new SimpleStringProperty(name);
        this.zutatenProperty = new SimpleStringProperty(zutaten);
        this.schritteProperty = new SimpleStringProperty(schritte);
        this.id = 0;
    }

    /**
     * Namensproperty fuer javafx
     * @return nameProperty
     */
    public StringProperty nameProperty() { return nameProperty; }

    /**
     * Zutatenproperty fuer javafx
     * @return nameProperty
     */
    public StringProperty zutatenProperty() { return zutatenProperty; }

    /**
     * Schritteproperty fuer javafx
     * @return nameProperty
     */
    public StringProperty schritteProperty() { return schritteProperty; }

    /**
     * Gibt id wieder
     * @return id
     */
    public int getID() {
        return this.id;
    }

    /**
     * Gibt name wieder
     * @return name
     */
    public String getName() {
        return nameProperty().get();
    }

    /**
     * Gibt zutaten wieder
     * @return zutaten
     */
    public String getZutaten() {
        return zutatenProperty().get();
    }

    /**
     * Gibt schritte wieder
     * @return schritte
     */
    public String getSchritte() {
        return schritteProperty().get();
    }

    /**
     * Aendert id
     * @param id Die einzigartige id
     */
    public void setID(int id) {
        this.id = id;
    }

    /**
     * Aendert name
     * @param name der neue Name
     */
    public void setName(String name) {
        nameProperty().set(name);
    }

    /**
     * Aendert zutaten
     * @param zutaten die neuen Zutaten
     */
    public void setZutaten(String zutaten) {
        zutatenProperty().set(zutaten);
    }

    /**
     * Aendert schritte
     * @param schritte die neuen Schritte
     */
    public void setSchritte(String schritte) {
        schritteProperty().set(schritte);
    }


    /**
     * init Methode fuer die custom Serialisierung
     */
    private void init() {
        this.nameProperty = new SimpleStringProperty();
        this.zutatenProperty = new SimpleStringProperty();
        this.schritteProperty = new SimpleStringProperty();
    }

    /**
     * Methode fuer die custom Serialisierung
     * @param oos der Stream zu dem geschrieben wird
     * @throws IOException wenn es einen Fehler beim schreiben des Objektes gab.
     */
    private void writeObject(ObjectOutputStream oos) throws IOException {
        oos.defaultWriteObject();
        oos.writeInt(getID());
        oos.writeUTF(nameProperty().getValueSafe());
        oos.writeUTF(zutatenProperty().getValueSafe());
        oos.writeUTF(schritteProperty().getValueSafe());
    }

    /**
     * Methode fuer die custom Serialisierung
     * @param ois der Stream von dem gelesen wird
     * @throws IOException wenn es einen Fehler beim lesen des Objektes gab.
     */
    private void readObject(ObjectInputStream ois) throws IOException {
        init();
        setID(ois.readInt());
        setName(ois.readUTF());
        setZutaten(ois.readUTF());
        setSchritte(ois.readUTF());
    }

}
