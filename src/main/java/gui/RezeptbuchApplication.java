package gui;

import javafx.application.Application;
import javafx.stage.Stage;

/**
 * Die Anwendung
 */
public class RezeptbuchApplication extends Application {

    /**
     * Erstellt ein neues Rezeptbuch und startet die GUI.
     * @param primaryStage das Hauptfenster
     */
    @Override
    public void start(Stage primaryStage) {
        primaryStage.setTitle("Rezeptbuch");
        RezeptViewController rezeptViewController = new RezeptViewController();
        rezeptViewController.takeOver(primaryStage); // Ersetzt die Scene der primaryStage
        primaryStage.show();
    }

    public static void main(String[] args) {
        System.out.println(System.getProperty("user.dir"));
        launch(args);
    }

}
