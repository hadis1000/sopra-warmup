package gui;


import javafx.scene.control.Alert;
import javafx.stage.Modality;

/**
 * ErrorAlert zeigt dem benutzer alle moeglichen Fehler.
 */
public class ErrorAlert {

    /**
     * Zeigt dem Benutzer eine Fehlermeldung
     * @param t Zu zeigender Fehler.
     */
    public static void display(Throwable t) {
        t.printStackTrace();
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.initModality(Modality.APPLICATION_MODAL);
        alert.setResizable(false);
        alert.setTitle("Error");
        alert.setHeaderText(null);
        alert.setContentText(t.getLocalizedMessage());
        alert.showAndWait();
    }

}
