package gui;

import javafx.scene.control.ListCell;
import model.Rezept;

/**
 * RezeptCell ist eine ListCell, die ein Rezept enthalten kann.
 */
public class RezeptCell extends ListCell<Rezept> {

    /**
     * Updatet den Zelltext.
     * @param rezept Ein Rezept.
     * @param empty Gibt an, ob die Zelle empty ist, oder nicht.
     */
    @Override
    protected void updateItem(Rezept rezept, boolean empty) {
        super.updateItem(rezept, empty);

        if(empty || rezept == null) {
            textProperty().unbind();
            setText(null);
        } else {
            textProperty().bind(rezept.nameProperty());
        }

    }

}
