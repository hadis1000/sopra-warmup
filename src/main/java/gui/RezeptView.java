package gui;

import javafx.collections.ObservableList;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.geometry.VPos;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.*;
import model.Rezept;

/**
 * Die Hauptview dieser Anwendung.
 */
public class RezeptView {

    private Scene scene;

    private BorderPane rootPane;

    // ListView, die Rezepte enhaelt.
    private ListView<Rezept> rezeptListView;

    // Die drei Textfelder, fuer Rezeptnamen, zutaten und schritte.
    private TextField nameField;
    private TextArea zutatenArea;
    private TextArea schritteArea;

    private Button neuButton;
    private Button speichernButton;
    private Button loeschenButton;

    /**
     * Initialisiert die View.
     * @param rezepte Eine Liste von Rezepten die in der ListView angezeit werden sollen.
     */
    public RezeptView(ObservableList<Rezept> rezepte) {
        this.rootPane = new BorderPane();
        this.rootPane.setTop(initRezeptPane(rezepte));
        this.rootPane.setCenter(initInputPane());
        this.rootPane.setBottom(initButtonPane());
        this.scene = new Scene(rootPane, 500, 600);
    }

    /**
     * @return Die Scene dieser View.
     */
    public Scene getScene() {
        return this.scene;
    }

    /**
     * @return Die ListView dieser View.
     */
    public ListView<Rezept> getRezeptListView() {
        return this.rezeptListView;
    }

    /**
     * @return Das TextField, welches fuer Namen zustaendig ist.
     */
    public TextField getNameField() {
        return nameField;
    }

    /**
     * @return Die TextArea, welche fuer die Zutaten zustaendig ist.
     */
    public TextArea getZutatenArea() {
        return zutatenArea;
    }

    /**
     * @return Die TextArea, welche fuer die Schritte zustaendig ist.
     */
    public TextArea getSchritteArea() {
        return schritteArea;
    }

    /**
     * @return Den Button, welcher fuer die Erstelung eines neuen Rezeptes zustaendig ist.
     */
    public Button getNeuButton() {
        return neuButton;
    }

    /**
     * @return Den Button, welcher fuer das Speichern aller Rezepte zustaendig ist.
     */
    public Button getSpeichernButton() {
        return speichernButton;
    }

    /**
     * @return Den Button, welcher fuer das Loeschen aller Rezepte zustaendig ist.
     */
    public Button getLoeschenButton() {
        return loeschenButton;
    }

    /**
     * Configuriert eine Pane, welche die ListView enthaelt.
     * @param rezepte Die Liste von Rezepten, welche angezeigt werden sollen.
     * @return Die configurierte Pane.
     */
    private ScrollPane initRezeptPane(ObservableList<Rezept> rezepte) {
        ScrollPane scrollPane = new ScrollPane();
        scrollPane.prefViewportHeightProperty().bind(this.rootPane.heightProperty().divide(2.5));
        scrollPane.setFitToWidth(true);

        this.rezeptListView = new ListView<>();
        this.rezeptListView.prefHeightProperty().bind(scrollPane.heightProperty());
        this.rezeptListView.setCellFactory(cb -> new RezeptCell());
        this.rezeptListView.setItems(rezepte);

        scrollPane.setContent(this.rezeptListView);

        return scrollPane;
    }

    /**
     * Configuriert eine Pane, welche die Input-textfelder/areas enthaelt.
     * @return Die configurierte Pane.
     */
    private GridPane initInputPane() {
        GridPane inputPane = new GridPane();
        inputPane.setPadding(new Insets(10,10,10,10));
        inputPane.setVgap(10);
        inputPane.setHgap(5);

        RowConstraints rc1 = new RowConstraints();
        RowConstraints rc2 = new RowConstraints();
        RowConstraints rc3 = new RowConstraints();
        rc2.setValignment(VPos.TOP);
        rc3.setValignment(VPos.TOP);
        rc2.prefHeightProperty().bind(inputPane.heightProperty());
        rc3.prefHeightProperty().bind(inputPane.heightProperty());
        inputPane.getRowConstraints().addAll(rc1, rc2, rc3);

        ColumnConstraints cc1 = new ColumnConstraints();
        cc1.setHalignment(HPos.RIGHT);

        ColumnConstraints cc2 = new ColumnConstraints();
        cc2.prefWidthProperty().bind(inputPane.widthProperty());
        inputPane.getColumnConstraints().addAll(cc1, cc2);

        this.nameField = new TextField();
        this.zutatenArea = new TextArea();
        this.schritteArea = new TextArea();

        Label nameLabel = new Label("Name:");
        Label zutatenLabel = new Label("Zutaten:");
        Label schritteLabel = new Label("Schritte:");
        nameLabel.setMinWidth(Region.USE_PREF_SIZE);
        zutatenLabel.setMinWidth(Region.USE_PREF_SIZE);
        schritteLabel.setMinWidth(Region.USE_PREF_SIZE);

        inputPane.add(nameLabel, 0, 0);
        inputPane.add(zutatenLabel, 0, 1);
        inputPane.add(schritteLabel, 0, 2);
        inputPane.add(nameField, 1, 0);
        inputPane.add(zutatenArea, 1, 1);
        inputPane.add(schritteArea, 1, 2);

        return inputPane;
    }

    /**
     * Configuriert eine Pane, welche die Buttons enthaelt.
     * @return Die configurierte Pane.
     */
    private FlowPane initButtonPane() {
        FlowPane buttonFlowPane = new FlowPane();
        buttonFlowPane.setPadding(new Insets(0, 5, 5, 5));
        buttonFlowPane.setHgap(5);
        buttonFlowPane.setVgap(5);

        speichernButton = new Button("Speichern");
        loeschenButton = new Button("Loeschen");
        neuButton = new Button("Neu");
        buttonFlowPane.getChildren().addAll(neuButton, speichernButton, loeschenButton);

        return buttonFlowPane;
    }

}
