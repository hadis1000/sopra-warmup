package gui;

import javafx.beans.binding.Bindings;
import javafx.collections.ListChangeListener;
import javafx.stage.Stage;
import model.Rezept;
import model.RezeptBuch;

import java.io.IOException;
import java.util.List;

/**
 * Controller der die RezeptView kontrolliert
 */
public class RezeptViewController {

    // Das Rezeptbuch, mit dem interagiert wird.
    private RezeptBuch rezeptBuch;
    // RezeptView, die gesteuert wird.
    private RezeptView rezeptView;

    /**
     * Erstellt eine RezeptView und initialisiert die EventListeners
     */
    public RezeptViewController() {
        try {
            this.rezeptBuch = new RezeptBuch();
        } catch (IOException e) {
            ErrorAlert.display(e);
        } catch (ClassNotFoundException e) {
            ErrorAlert.display(e);
        }
        this.rezeptView = new RezeptView(rezeptBuch.getRezepte());
        listen();
    }

    /**
     * @return Die zu diesem Controller gehoerige RezeptView
     */
    public RezeptView getRezeptView() {
        return this.rezeptView;
    }

    /**
     * Uebernimmt eine Stage.
     * @param stage die zu uebernehmende Stage.
     */
    public void takeOver(Stage stage) {
        stage.setScene(getRezeptView().getScene());
        stage.setOnCloseRequest(e -> saveRequest());
    }

    /**
     * Definiert verhalten fuer die relevanten Events.
     */
    private void listen() {
        // bindet immer das selected item an die texfelder.
        rezeptView.getRezeptListView().getSelectionModel().selectedItemProperty()
                .addListener((observable, oldValue, newValue) -> setDisplayedRezept(oldValue, newValue));
        // speichert
        rezeptView.getSpeichernButton().setOnAction(e -> saveRequest());
        // loescht
        rezeptView.getLoeschenButton().setOnAction(e -> loeschRequest());
        // erzeugt ein neues Rezept
        rezeptView.getNeuButton().setOnAction(e -> rezeptBuch.newRezept());
        // waehlt ein neu hinzugefuegtes rezept aus
        rezeptView.getRezeptListView().getItems()
                .addListener((ListChangeListener<? super Rezept>) this::selectionUpdate);

        var selectedItemProperty = getRezeptView().getRezeptListView().getSelectionModel().selectedItemProperty();
        getRezeptView().getNameField().disableProperty().bind(Bindings.isNull(selectedItemProperty));
        getRezeptView().getZutatenArea().disableProperty().bind(Bindings.isNull(selectedItemProperty));
        getRezeptView().getSchritteArea().disableProperty().bind(Bindings.isNull(selectedItemProperty));
    }

    /**
     * Bindet ein Rezept bidirektional an die entsprechenden Textfelder der GUI.
     * @param selected Das Rezept das bidirektional gebunden werden soll.
     */
    private void bibindInput(Rezept selected) {
        getRezeptView().getNameField().textProperty().bindBidirectional(selected.nameProperty());
        getRezeptView().getZutatenArea().textProperty().bindBidirectional(selected.zutatenProperty());
        getRezeptView().getSchritteArea().textProperty().bindBidirectional(selected.schritteProperty());
    }

    /**
     * Entbindet ein Rezept bidirektional von die entsprechenden Textfelder der GUI.
     * @param unselected Das Rezept das bidirektional entbunden werden soll.
     */
    private void unbibindInput(Rezept unselected) {
        getRezeptView().getNameField().textProperty().unbindBidirectional(unselected.nameProperty());
        getRezeptView().getZutatenArea().textProperty().unbindBidirectional(unselected.zutatenProperty());
        getRezeptView().getSchritteArea().textProperty().unbindBidirectional(unselected.schritteProperty());
    }

    /**
     * Leert alle Textfelder.
     */
    private void clearInput() {
        getRezeptView().getNameField().setText("");
        getRezeptView().getZutatenArea().setText("");
        getRezeptView().getSchritteArea().setText("");
    }

    /**
     * Bindet oldRezept an die GUI und entbindet newRezept von der GUI.
     * @param oldRezept das zu entbindende Rezept
     * @param newRezept das zu bindende Rezept
     */
    private void setDisplayedRezept(Rezept oldRezept, Rezept newRezept) {
        if (oldRezept != null) {
            unbibindInput(oldRezept);
        }
        if (newRezept != null) {
            bibindInput(newRezept);
        } else {
            clearInput();
        }
    }

    /**
     * Speichert alle Rezepte.
     */
    private void saveRequest() {
        System.out.println("speichern");
        try {
            rezeptBuch.save();
        } catch (IOException e) {
            ErrorAlert.display(e);
        }
    }

    /**
     * Loescht das aktuell ausgewaehlte Rezept
     */
    private void loeschRequest() {
        System.out.println("loeschen");
        Rezept selected = getRezeptView().getRezeptListView().getSelectionModel().getSelectedItem();
        if (selected != null) {
            rezeptBuch.remove(selected);
        }
    }

        private void selectionUpdate(ListChangeListener.Change<? extends Rezept> change) {
         while (change.next()) {
             if (change.wasAdded()) {
                 List<? extends Rezept> neueRezepte = change.getAddedSubList();
                 Rezept neustesRezept = neueRezepte.get(neueRezepte.size() - 1);
                 getRezeptView().getRezeptListView().getSelectionModel().select(neustesRezept);
             }
         }
     }
}
