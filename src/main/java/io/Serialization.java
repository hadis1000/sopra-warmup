package io;

import model.Rezept;

import java.io.*;
import java.util.*;

/**
 * Speichert und Liest gespeicherte Rezepte
 */
public class Serialization {

    // Der ordner in dem das Programm gestartet wird
    private static final String runDir = System.getProperty("user.dir");
    // Der rezeptordner
    private static final String rezeptDir = runDir + File.separator + "rezepte" + File.separator;
    private File rezeptOrdner;

    /**
     * Initialisiert eine neue instanz
     */
    public Serialization() {
        this.rezeptOrdner = new File(rezeptDir);
        initOrdner();
    }

    /**
     * liest alle Rezepte aus dem Rezeptordner und gibt dieser wieder.
     * @return Gibt alle Rezepte aus dem Rezeptordner wieder
     * @throws IOException wenn es einen Fehler beim lesen der Rezepte gab.
     * @throws ClassNotFoundException wenn sich zum Beispiel die serialisierte Klasse geaendert hat.
     */
    public List<Rezept> getRezepte() throws IOException, ClassNotFoundException {
        initOrdner();
        List<Rezept> rezepte = new ArrayList<>();
        File[] rezeptOrdnerInhalt = rezeptOrdner.listFiles();
        if(rezeptOrdnerInhalt == null || rezeptOrdnerInhalt.length == 0) {
            return rezepte;
        } else {
            Arrays.sort(rezeptOrdnerInhalt);
            for(File rezeptDatei : rezeptOrdnerInhalt) {
                try (ObjectInputStream ois = new ObjectInputStream(new FileInputStream(rezeptDatei))) {
                    Rezept rezept = (Rezept) ois.readObject();
                    rezepte.add(rezept);
                }
            }
        }

        return rezepte;
    }


    /**
     * Schreibt alle Rezepte einer Liste in den Rezeptordner.
     * @param rezepte zu schreibende Rezepte.
     * @throws IOException wenn ein rezept nicht geschrieben werden konnte
     */
    public void write(List<Rezept> rezepte) throws IOException {
        for (Rezept r : rezepte) {
            if(r.getID() == 0) {
                r.setID(getNextID());
            }
            write(r);
        }
    }

    /**
     * Schreibt ein einzelnes Rezept in den Rezeptordner.
     * @param rezept zu schreibendes Rezept.
     * @throws IOException wenn das Rezept nicht geschrieben werden konnte.
     */
    public void write(Rezept rezept) throws IOException {
        initOrdner();
        if (rezeptOrdner.exists()) {
            try (ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(rezeptDir + rezept.getID(), false))) {
                oos.writeObject(rezept);
            }
        } else {
            write(rezept); // scary
        }
    }


    /**
     * Initialisiert den Rezeptordner.
     */
    private void initOrdner() {
        File rezeptOrdner = new File(rezeptDir);
        if (!rezeptOrdner.exists()) {
            rezeptOrdner.mkdirs();
        }
    }

    /**
     * Loescht das gegebene Rezept von der Festplatte.
     * @param rezept zu loeschendes Rezept.
     */
    public void delete(Rezept rezept) {
        File rezeptDatei = new File(rezeptDir + File.separator + rezept.getID());
        if(rezeptDatei.exists()) {
            rezeptDatei.delete();
        }
    }

    /**
     * @return naechste Rezept-id
     * @throws IOException wenn der Rezeptordner nicht korrekt strukturiert ist.
     */
    private int getNextID() throws IOException {
        return getMaxID() + 1;
    }

    /**
     * @return maximale ID im Rezeptordner
     * @throws IOException wenn der Rezeptordner nicht korrekt strukturiert ist.
     */
    private int getMaxID() throws IOException {
        File rezeptOrdner = new File(rezeptDir);
        File[] rezeptDateien = rezeptOrdner.listFiles();
        if(rezeptDateien != null && rezeptDateien.length != 0) {
            return Arrays.stream(rezeptDateien)
                    .mapToInt(f -> Integer.parseInt(f.getName()))
                    .max()
                    .orElseThrow(() -> new IOException("Die Ordnerstruktur fuer Rezepte scheint nicht in ordnung zu sein."));
        } else {
            return 0;
        }
    }

}
